import json
import datetime
import pdfkit

time = datetime.datetime.utcnow()


def save_to_db(entity):
    print("saved to database")


def get_json_file_name():
    return "{}.json".format(time.timestamp())


def save_big_json_file(_data):
    data_json = json.dumps(_data)
    path = "./bucket/{}"
    filename = path.format(get_json_file_name())
    f = open(filename, "x")
    f.write(data_json)
    f.close()
    return filename


def get_layout_template():
    layout_file = "./html/layout.html"
    f = open(layout_file)
    layout = f.read()
    f.close()
    return layout


def generate_html(_data):
    template = get_layout_template()
    content = ""
    row = '''
    <tr style="height: 25px">
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
        <td>{}</td>
    </tr>
    '''
    for _id in _data:
        content = content + str(
            row.format(
                _data[_id]["ID"],
                _data[_id]["ID"],
                _data[_id]["NAME"],
                _data[_id]["CARD_NUMBER"],
                _data[_id]["TOTAL_TRANSACTIONS"],
                _data[_id]["AMOUNT"]
            ))
    return template.format(time.utcnow(), 20, 10, 9000, content)


def save_big_pdf_file(_html):
    options = {
        'page-size': 'A4',
        # 'margin-top': '0.5in',
        # 'margin-right': '0.5in',
        # 'margin-bottom': '0.5in',
        # 'margin-left': '0.5in',
    }
    pdfkit.from_string(_html, "./pdf/output.pdf", options=options)
    print("pdf file created")


def big_file_parse(filename):
    clients = {}
    try:
        f = open(filename, "r")
        content = f.read()
        if ";" in str(content):
            _clients = content.split(";")
            for _client in _clients:
                if ":" in str(_client):
                    _pairs = _client.split(",")
                    _id = ""
                    client = {}
                    for _pair in _pairs:
                        if ":" in str(_pair):
                            pair = _pair.split(":")
                            if pair[0] == "ID":
                                _id = pair[1]
                            client[pair[0]] = pair[1]
                    clients[_id] = client
            return clients

    except FileNotFoundError:
        print("el arquivo no existe, haz algo")
    finally:
        pass


if __name__ == '__main__':
    data = big_file_parse("./input.txt")
    # save_big_json_file(data)
    # save_big_pdf_file(data)
    html = generate_html(data)
    save_big_pdf_file(html)
    print("Done!")
